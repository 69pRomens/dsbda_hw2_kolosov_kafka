package ru.mephi.dsbda;

import ru.mephi.dsbda.database.migrations.CassandraTables;
import ru.mephi.dsbda.database.seeds.DatabaseSeeder;
import ru.mephi.dsbda.kafka.MainConsumer;
import ru.mephi.dsbda.kafka.MainProducer;
import ru.mephi.dsbda.models.Citizen;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class Router {
    private enum Routes {
        SEEDS,
        PRODUCE,
        CONSUME
    }


    /**
     * Router for actions
     * @param route
     */
    static void routes(String route){
        try {
            Routes routes = Routes.valueOf(route.toUpperCase().substring(2));

            switch (routes) {
                case SEEDS:
                    try {
                        Router.seeds();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case PRODUCE:
                    Router.produce();
                    break;
                case CONSUME:
                    Router.consume();
                    break;
                default:
                    System.out.println("Error: route is not found!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate tables and data
     * @throws SQLException
     */
    private static void seeds() throws SQLException {
        DatabaseSeeder.run();

        System.out.println("SALARIES:");
        ArrayList<Citizen> salaries = Citizen.salaries();
        for(Citizen salary : salaries){
            System.out.println(salary);
        }

        System.out.println("TRIPS:");
        ArrayList<Citizen> trips = Citizen.trips();
        for(Citizen trip : trips){
            System.out.println(trip);
        }

        CassandraTables.up();
    }


    /**
     * Push data to producers
     */
    private static void produce() {
        MainProducer producer = new MainProducer();

        try {
            List<Citizen> combined = new ArrayList<Citizen>();
            combined.addAll(Citizen.salaries());
            combined.addAll(Citizen.trips());
            producer.run(combined);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Pull data from consumers send to Cassandra
     */
    private static void consume() {
        MainConsumer consumer = new MainConsumer();
        consumer.run();
    }
}
