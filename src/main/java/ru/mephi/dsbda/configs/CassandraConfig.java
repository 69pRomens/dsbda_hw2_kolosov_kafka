package ru.mephi.dsbda.configs;

public final class CassandraConfig  extends Config {

    private String host;
    private String port;

    public CassandraConfig() {
        host = dotenv.get("CASSANDRA_HOST");
        port = dotenv.get("CASSANDRA_PORT");
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }
}