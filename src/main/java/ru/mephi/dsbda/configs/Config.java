package ru.mephi.dsbda.configs;

import io.github.cdimascio.dotenv.Dotenv;

/**
 * Take data from .env file
 */
class Config {
    final Dotenv dotenv = Dotenv.load();
}
