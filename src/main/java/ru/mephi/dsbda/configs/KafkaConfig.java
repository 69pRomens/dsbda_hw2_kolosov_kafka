package ru.mephi.dsbda.configs;

public final class KafkaConfig extends Config {

    private String host;
    private String bootstrapPort;
    private String listenerPort;
    private String groupId;
    private String topic;

    public KafkaConfig() {
        host = dotenv.get("KAFKA_HOST");
        bootstrapPort = dotenv.get("KAFKA_BOOTSTRAP_PORT");
        listenerPort = dotenv.get("KAFKA_LISTENER_PORT");
        groupId = dotenv.get("KAFKA_GROUP_ID");
        topic = dotenv.get("KAFKA_TOPIC");
    }

    public String getHost() {
        return host;
    }

    public String getBootstrapPort() {
        return bootstrapPort;
    }

    public String getListenerPort() {
        return listenerPort;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getTopic() {
        return topic;
    }
}
