package ru.mephi.dsbda.configs;

public final class MysqlConfig extends Config {

    private String host;
    private String port;
    private String database;
    private String username;
    private String password;

    public MysqlConfig() {
        host = dotenv.get("MYSQL_HOST");
        port = dotenv.get("MYSQL_PORT");
        database = dotenv.get("MYSQL_DATABASE");
        username = dotenv.get("MYSQL_USERNAME");
        password = dotenv.get("MYSQL_PASSWORD");
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
