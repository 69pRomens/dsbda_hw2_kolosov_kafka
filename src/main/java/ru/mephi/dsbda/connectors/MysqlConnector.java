package ru.mephi.dsbda.connectors;

import ru.mephi.dsbda.configs.MysqlConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class MysqlConnector {
    private static final MysqlConfig config = new MysqlConfig();

    private final static String driver = "com.mysql.cj.jdbc.Driver";

    private Connection connection;

    public MysqlConnector(){
        try {
            Class.forName(driver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Take connection for MySql
     * @return
     */
    public Connection getConnection() {
        try {
            String uri = "jdbc:mysql://" + config.getHost() + ":" + config.getPort() + "/" + config.getDatabase()
                + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            connection = DriverManager.getConnection(uri, config.getUsername(), config.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    /**
     * Close connection
     */
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
