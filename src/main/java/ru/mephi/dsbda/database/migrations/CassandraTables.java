package ru.mephi.dsbda.database.migrations;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import ru.mephi.dsbda.configs.CassandraConfig;

public class CassandraTables {

    /**
     * Create all tables for Cassandra
     */
    public static void up() {
        CassandraConfig cassandraConfig = new CassandraConfig();

        String serverIP = cassandraConfig.getHost();
        String keyspace = "citizens";

        Cluster cluster = Cluster.builder()
                .addContactPoints(serverIP)
                .build();

        Session session = cluster.connect(keyspace);

        String statement = "";

        statement = "DROP TABLE IF EXISTS citizens.salaries;";
        session.execute(statement);

        statement = "DROP TABLE IF EXISTS citizens.trips;";
        session.execute(statement);

        statement = "DROP TABLE IF EXISTS citizens.statistics;";
        session.execute(statement);

        statement = "CREATE TABLE citizens.trips (uuid UUID PRIMARY KEY, id int, passport text, birthday timestamp, month text, trips int);";
        session.execute(statement);

        statement = "CREATE TABLE citizens.salaries ( uuid UUID PRIMARY KEY, id int, passport text, birthday timestamp, month text, salary text);";
        session.execute(statement);

        statement = "CREATE TABLE citizens.statistics ( uuid UUID PRIMARY KEY, age_category text, average_salary double, average_trips double);";
        session.execute(statement);

        session.close();
    }
}
