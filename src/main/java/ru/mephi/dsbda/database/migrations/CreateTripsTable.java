package ru.mephi.dsbda.database.migrations;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.SQLException;

public class CreateTripsTable {
    /**
     * Create Trips table
     * @throws SQLException
     */
    public static void up() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        mysql.getConnection()
                .createStatement()
                .executeUpdate("CREATE TABLE trips (" +
                        "id INT AUTO_INCREMENT NOT NULL," +
                        "passport VARCHAR(80) NOT NULL," +
                        "month VARCHAR(80) NOT NULL," +
                        "trips INT NOT NULL DEFAULT 0," +
                        "PRIMARY KEY (id)" +
                        ")");

        mysql.closeConnection();

    }

    /**
     * Drop trips table
     * @throws SQLException
     */
    public static void down() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        mysql.getConnection()
                .createStatement()
                .executeUpdate("DROP TABLE trips");

        mysql.closeConnection();
    }
}
