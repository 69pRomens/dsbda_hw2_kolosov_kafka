package ru.mephi.dsbda.database.seeds;

import ru.mephi.dsbda.connectors.MysqlConnector;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SalarySeeder {

    /**
     * Generate data for MySql
     * @throws SQLException
     */
    public static void generate() throws SQLException {
        MysqlConnector mysql = new MysqlConnector();

        Random random = new Random();
        List<String> months = Arrays
                .asList("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        String passport = "";
        String month = "";
        String salary = "";

        ResultSet result;

        result = mysql.getConnection()
                .createStatement()
                .executeQuery("SELECT * FROM citizens");

        String query = "INSERT INTO salaries (passport, month, salary) VALUES (?, ?, ?)";
        PreparedStatement statement = mysql.getConnection().prepareStatement(query);

        while(result.next()) {
            Timestamp birthday = result.getTimestamp("birthday");

            if (birthday.before(Timestamp.valueOf("1999-01-01 00:00:00"))) {
                passport = result.getString("passport");
                month = months.get(random.nextInt(months.size()));
                salary = Integer.toString(25000 + random.nextInt(220000));

                statement.setString(1, passport);
                statement.setString(2, month);
                statement.setString(3, salary);

                statement.addBatch();
            }
        }

        statement.executeBatch();

        mysql.closeConnection();
    }
}
