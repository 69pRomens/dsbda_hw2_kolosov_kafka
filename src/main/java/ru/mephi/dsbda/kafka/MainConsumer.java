package ru.mephi.dsbda.kafka;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import ru.mephi.dsbda.configs.CassandraConfig;
import ru.mephi.dsbda.configs.KafkaConfig;
import ru.mephi.dsbda.models.Citizen;

import java.util.Collections;
import java.util.Properties;

public class MainConsumer {

    private final KafkaConfig config = new KafkaConfig();
    private final CassandraConfig cassandraConfig = new CassandraConfig();

    private Properties props = new Properties();

    /**
     * Init MainConsumer
     */
    public MainConsumer () {
        String deserializer = StringDeserializer.class.getName();
        props.put("bootstrap.servers", config.getHost() + ":" + config.getBootstrapPort());
        props.put("group.id", config.getGroupId());
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", deserializer);
        props.put("value.deserializer", deserializer);
    }

    /**
     * Process for pull data from Kafka and push to Cassandra
     */
    public void run() {
        String serverIP = cassandraConfig.getHost();
        String keyspace = "citizens";

        Cluster cluster = Cluster.builder()
                .addContactPoints(serverIP)
                .build();

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(config.getTopic()));

        int miss = 0;

        while (miss < 15) {
            ConsumerRecords<String, String> records = consumer.poll(1000);

            if (records.isEmpty()) {
                miss++;
                continue;
            }

            Session session = cluster.connect(keyspace);

            for (ConsumerRecord<String, String> record : records) {
                System.out.println(record.value());
                Citizen citizen = new Citizen(record.value());

                if (citizen.getSalary().equals("null")) {
                    String cqlStatementC = "INSERT INTO citizens.trips (uuid, id, passport, birthday, month, trips) " +
                            "VALUES (" +
                            "uuid()" +
                            "," + citizen.getId() +
                            ",'" + citizen.getPassport() + "'" +
                            ",'" + citizen.getBirthday() + "'" +
                            ",'" + citizen.getMonth() + "'" +
                            "," + citizen.getTrips() +
                            ")";

                    session.execute(cqlStatementC);
                } else {
                    String cqlStatementC = "INSERT INTO citizens.salaries (uuid, id, passport, birthday, month, salary) " +
                            "VALUES (" +
                            "uuid()" +
                            "," + citizen.getId() +
                            ",'" + citizen.getPassport() + "'" +
                            ",'" + citizen.getBirthday() + "'" +
                            ",'" + citizen.getMonth() + "'" +
                            ",'" + citizen.getSalary() + "'" +
                            ")";

                    session.execute(cqlStatementC);
                }

            }

            session.close();
        }

    }
}
