package ru.mephi.dsbda.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import ru.mephi.dsbda.configs.KafkaConfig;
import ru.mephi.dsbda.models.Citizen;


import java.util.*;

public class MainProducer {

    private final KafkaConfig config = new KafkaConfig();

    private Properties props = new Properties();


    /**
     * Init MainProducer
     */
    public MainProducer () {
        String serializer = StringSerializer.class.getName();
        props.put("bootstrap.servers", config.getHost() + ":" + config.getBootstrapPort());
        props.put("advertised.listeners", config.getHost() + ":" + config.getListenerPort());
        props.put("key.serializer", serializer);
        props.put("value.serializer", serializer);
    }

    /**
     * Proccess for pushing data from MySQL to Kafka
     * @param citizens
     * @return
     */
    public int run (List<Citizen> citizens) {
        final Iterator<Citizen> iterator = citizens.iterator();

        int count = 0;

        try {
            Producer<Object, String> producer = new KafkaProducer<>(props);
            while(iterator.hasNext()){
                // Send the sentence to the test topic
                producer.send(new ProducerRecord<>(config.getTopic(), iterator.next().toString()));
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }
}
