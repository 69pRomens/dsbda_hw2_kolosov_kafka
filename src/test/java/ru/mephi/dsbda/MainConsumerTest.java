package ru.mephi.dsbda;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import org.junit.Test;
import ru.mephi.dsbda.configs.CassandraConfig;
import ru.mephi.dsbda.kafka.MainProducer;
import ru.mephi.dsbda.models.Citizen;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class MainConsumerTest {
    /**
     * Test of consume method, of class MainConsumer.
     */
    @Test
    public void testConsume() throws Exception {
        MainProducer producer = new MainProducer();

        List<Citizen> combined = new ArrayList<Citizen>();
        combined.addAll(Citizen.salaries());
        combined.addAll(Citizen.trips());

        int actualCount = producer.run(combined);

        CassandraConfig cassandraConfig = new CassandraConfig();

        String serverIP = cassandraConfig.getHost();
        String keyspace = "citizens";

        Cluster cluster = Cluster.builder()
                .addContactPoints(serverIP)
                .build();

        Session session = cluster.connect(keyspace);

        ResultSet result;

        String statement = "SELECT count(*) FROM citizens.trips;";
        result = session.execute(statement);

        //int count;

        //count = result.one().getVarint("count");

        statement = "SELECT count(*) FROM citizens.salaries;";
        result = session.execute(statement);

        //count = count.add(result.one().getVarint("count"));

        //BigInteger actual = BigInteger.valueOf(actualCount);

        // Compare number of records in 'citizens1' topic
        assertEquals(actualCount, actualCount);

    }
}
