package ru.mephi.dsbda;

import org.junit.Test;
import ru.mephi.dsbda.kafka.MainProducer;
import ru.mephi.dsbda.models.Citizen;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class MainProducerTest {
    /**
     * Test of produce method, of class MainProducer.
     */
    @Test
    public void testProduce() throws Exception {
        MainProducer producer = new MainProducer();

        List<Citizen> combined = new ArrayList<Citizen>();
        combined.addAll(Citizen.salaries());
        combined.addAll(Citizen.trips());

        int actualCount = producer.run(combined);

        // Compare number of records in 'citizens1' topic
        assertEquals(combined.size(), actualCount);
    }
}
